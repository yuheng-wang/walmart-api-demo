package com.example.walmartapidemo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.walmartapidemo.domain.DcWalmartProducts;

/**
 * @author Charles
 * @date 2024/2/2
 */
public interface WalmartProductsMapper extends BaseMapper<DcWalmartProducts> {
}
