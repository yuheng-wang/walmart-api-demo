package com.example.walmartapidemo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.walmartapidemo.domain.DcWalmartProducts;
import com.example.walmartapidemo.domain.DcWalmartSellers;

/**
 * @author Charles
 * @date 2024/2/2
 */
public interface WalmartSellersMapper extends BaseMapper<DcWalmartSellers> {
}
