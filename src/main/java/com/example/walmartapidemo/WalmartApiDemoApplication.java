package com.example.walmartapidemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WalmartApiDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(WalmartApiDemoApplication.class, args);
    }

}
