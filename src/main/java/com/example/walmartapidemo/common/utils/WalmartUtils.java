package com.example.walmartapidemo.common.utils;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.http.Method;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson2.JSON;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.example.walmartapidemo.common.constant.WalmartConstant;
import com.example.walmartapidemo.domain.DcWalmartSellers;
import com.example.walmartapidemo.dto.DcWalmartItemParams;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

@Slf4j
public class WalmartUtils {
    private static List<Map<String, String>> PARAM_LIST = new ArrayList<>();
    //private static String DATE_TIME51 ="2021-07-01T00:00:00Z";  //UTC时间

    private static Map<String, String> ACKNOWLEDGED_MAP = new HashMap<>();
    private static Map<String, String> SHIPPED_MAP = new HashMap<>();
    private static Map<String, String> SHIPPED_MAP_2 = new HashMap<>();

    private static Map<String, String> CA_MAP = new HashMap<>();

    private static String LIMIT = "200";

    static {
//        SHIPPED_MAP.put("status", "Shipped");
        SHIPPED_MAP.put("shipNodeType", "WFSFulfilled");
        SHIPPED_MAP.put("limit", LIMIT);

//        SHIPPED_MAP_2.put("status", "Delivered");
        SHIPPED_MAP_2.put("shipNodeType", "SellerFulfilled");
        SHIPPED_MAP_2.put("limit", LIMIT);

//        ACKNOWLEDGED_MAP.put("status", "Acknowledged");
//        ACKNOWLEDGED_MAP.put("shipNodeType", "SellerFulfilled");
        ACKNOWLEDGED_MAP.put("limit", LIMIT);

//        CA_MAP.put("status", "Shipped");
        CA_MAP.put("limit", LIMIT);

        PARAM_LIST.add(SHIPPED_MAP);
        PARAM_LIST.add(SHIPPED_MAP_2);
//
//        PARAM_LIST.add(ACKNOWLEDGED_MAP);

    }

    /**
     * 获取可用的报告日期列表
     *
     * @param accountId 店铺ID
     * @author senghor
     * @date 2022/5/12 17:20
     */
    public static List<String> getAvailableReconReportDatesList(Long accountId) {
        String url = WalmartConstant.BASE_URL + WalmartConstant.AVAILABLE_RECON_REPORT_DATES_URL;
        JSONObject json = JSONUtil.parseObj(WalmartRequest.request(url, Method.GET, accountId));
        return json.getJSONArray("availableApReportDates").toList(String.class);
    }

    /**
     * 生成报告
     *
     * @param accountId 店铺ID
     * @param date      使用获取可用的报告日期列表返回的日期
     * @author senghor
     * @date 2022/5/12 17:20
     */
    public static String createReport(Long accountId, String date) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("reportDate",date);
        map.put("reportVersion","v1");
        // WalmartRequest.request(url, Method.GET, accountId);
        return WalmartRequest.request(WalmartConstant.BASE_URL + WalmartConstant.RECON_REPORT_URL,Method.GET, accountId, map);
    }

    /**
     * 获取订单详情
     *
     * @param purchaseOrderId 采购订单ID
     * @param accountId       店铺ID
     * @author senghor
     * @date 2022/5/12 17:20
     */
    public static String getOrderDetails(String purchaseOrderId, Long accountId) {
        return getOrderDetails(purchaseOrderId, accountId, false, false);
    }

    /**
     * 获取订单详情
     *
     * @param accountId       店铺ID
     * @param purchaseOrderId 采购订单ID
     * @param productInfo     是否返回产品信息 boolean
     * @param replacementInfo 是否返回替换订单信息 boolean
     * @author senghor
     * @date 2022/5/12 17:20
     */
    public static String getOrderDetails(String purchaseOrderId, Long accountId, Boolean productInfo, Boolean replacementInfo) {
        DcWalmartSellers walmartSellers = WalmartRequest.getShopSettings(accountId);
        String url = WalmartConstant.BASE_URL;
        Map<String, Object> paramMap = new HashMap<>();
        if (walmartSellers.getSite().equals("US")) {
            url = url + WalmartConstant.GET_ORDER_US_URL.replace("{purchaseOrderId}", purchaseOrderId);
            paramMap.put("productInfo", productInfo);
            paramMap.put("replacementInfo", replacementInfo);
        } else if (walmartSellers.getSite().equals("CA")) {
            url = url + WalmartConstant.GET_ORDER_CA_URL.replace("{purchaseOrderId}", purchaseOrderId);
            paramMap.put("productInfo", productInfo);
        }
        return WalmartRequest.request(url, Method.GET, accountId, paramMap);
    }


    /**
     * @param accountId             店铺帐号id
     * @param lastModifiedStartDate 上次修改开始日期	  us
     * @param lastModifiedEndDate   上次修改结束日期	us
     * @param createdStartDate      创建时间  ca  (ca站点没有修改时间参数)
     * @return
     */
    public static List<String> getOrderByTime(Long accountId, String lastModifiedStartDate, String lastModifiedEndDate, String createdStartDate) {
        List<String> orderStrs = new ArrayList<>();
        DcWalmartSellers walmartSellers = WalmartRequest.getShopSettings(accountId);
        String url = WalmartConstant.BASE_URL;

//        DateTime dateTimeStart = DateUtil.offsetHour(lastModifiedStartDate);
//        DateTime dateTimeEnd = DateUtil.offsetHour(lastModifiedEndDate);
//        DateTime dateTimeCreated = DateUtil.offsetHour(lastModifiedStartDate);

        //将时间
        if (walmartSellers.getSite().equals("US")) {
            url = url + WalmartConstant.GET_ORDER_US_URL;

            //传参 修改时间 参数 会默认带上订单创建时间 因此要自己带上订单创建时间
            for (Map<String, String> paramMap : PARAM_LIST) {

//                DateTime localTimeStart = DateUtil.offsetHour(lastModifiedStartDate, -8);
//                DateTime localTimeEnd = DateUtil.offsetHour(dateTimeEnd, -8);


                setCreatedStartDateByUS(paramMap, lastModifiedStartDate);

                //有些订单未更新状态 将更新开始时间自发货往前推两天  平台发货推前半天
                String shipNodeType = MapUtils.getString(paramMap, "shipNodeType");
                if ("WFSFulfilled".equals(shipNodeType)) {
                    DateTime localTimeEnd = DateUtil.offsetHour(new DateTime(lastModifiedEndDate), -12);
                    paramMap.put("lastModifiedStartDate", localTimeEnd.toString(DatePattern.UTC_PATTERN));
                } else if ("SellerFulfilled".equals(shipNodeType)) {
                    DateTime localTimeEnd = DateUtil.offsetDay(new DateTime(lastModifiedEndDate), -2);
                    paramMap.put("lastModifiedStartDate", localTimeEnd.toString(DatePattern.UTC_PATTERN));
                }
                paramMap.put("lastModifiedEndDate", lastModifiedEndDate);
                String json = WalmartRequest.request(url, Method.GET, accountId, paramMap);
                if (StringUtils.isNotEmpty(json)) {
                    isPullComplete(json, orderStrs, accountId, url);
                }
            }
        } else if (walmartSellers.getSite().equals("CA")) {

            //if (StringUtils.isNotEmpty(createdStartDate)) {
                //DateTime localTimeCreated = DateUtil.offsetHour(dateTimeCreated, -8);
//                DateTime localTimeStartDate= DateUtil.offsetMonth(new DateTime(lastModifiedEndDate), -1);
                //创建时间在一个月前的订单数据
                CA_MAP.put("createdStartDate", lastModifiedStartDate);
                CA_MAP.put("createdEndDate", lastModifiedEndDate);
            //}
            url = url + WalmartConstant.GET_ORDER_CA_URL;
            String json = WalmartRequest.request(url, Method.GET, accountId, CA_MAP);

            if (StringUtils.isNotEmpty(json)) {
                isPullComplete(json, orderStrs, accountId, url);
            }
        }

        return orderStrs;
    }

    /**
     * US站点 查询参数赋值订单创建时间
     *
     * @param paramMap
     * @param lastModifiedStartDate
     */
    private static void setCreatedStartDateByUS(Map<String, String> paramMap, String lastModifiedStartDate) {

        DateTime dateTime51 = new DateTime();

        DateTime dateTime = new DateTime(lastModifiedStartDate);
        DateTime time = dateTime.offset(DateField.MONTH, -3);
        DateTime offset = dateTime51.offset(DateField.MONTH, -6);
        if (time.compareTo(offset) > 0) {
            paramMap.put("createdStartDate", time.toString(DatePattern.UTC_PATTERN));
        } else {
            paramMap.put("createdStartDate", offset.toString(DatePattern.UTC_PATTERN));
        }
    }

    /**
     * 获取订单  测试
     *
     * @param accountId 店铺ID
     * @author senghor
     * @date 2022/5/12 17:20
     */
    public static List<String> getOrder(Long accountId) {
        Map<String, Object> paramMap = new HashMap<>();
        List<String> orderStrs = new ArrayList<>();
        DcWalmartSellers walmartSellers = WalmartRequest.getShopSettings(accountId);
        String url = WalmartConstant.BASE_URL;
        if (walmartSellers.getSite().equals("US")) {
            url = url + WalmartConstant.GET_ORDER_US_URL;
        } else if (walmartSellers.getSite().equals("CA")) {
            paramMap.put("createdStartDate", "2022-05-28");
            paramMap.put("status", "Shipped");
            paramMap.put("limit", "200");

            url = url + WalmartConstant.GET_ORDER_CA_URL;
            String request = WalmartRequest.request(url, Method.GET, accountId, paramMap);
            //查询该请求数据请求完成了没有
            isPullComplete(request, orderStrs, accountId, walmartSellers.getSite());

        }
        return orderStrs;
    }

    /**
     * @param request   返回的单条数据
     * @param orderStrs 返回数据集合
     */
    private static void isPullComplete(String request, List<String> orderStrs, Long accountId, String url) {
        try {
            orderStrs.add(request);
            JSONObject json = JSONUtil.parseObj(request);
            String str = json.getJSONObject("list").getJSONObject("meta").getStr("nextCursor");
            if (StringUtils.isNotEmpty(str)) {
                String nextUrl = url + str;
                String request1 = WalmartRequest.request(nextUrl, Method.GET, accountId, null);
                if (StringUtils.isNotEmpty(request1)) {
                    isPullComplete(request1, orderStrs, accountId, url);
                }
            }
        }catch (Exception e){
            log.error("walmart异常数据为:{},请求路径为:{}",request,url);
            log.error("获取walmart数据异常:"+e.getMessage(),e);
            throw new ServiceException("获取walmart数据异常:"+e);
        }

    }


    /**
     * 获取商品
     *
     * @param accountId 店铺ID
     * @author senghor
     * @date 2022/5/12 17:20
     */
    public static List<String> getProductsTest(Long accountId) {
        Map<String, Object> paramMap = new HashMap<>();
        List<String> productStrs = new ArrayList<>();
//        paramMap.put("includeDetails", Boolean.TRUE);
        paramMap.put("nextCursor", "*");
        paramMap.put("publishedStatus", "SYSTEM_PROBLEM");
        paramMap.put("limit", "30");

        DcWalmartSellers walmartSellers = WalmartRequest.getShopSettings(accountId);
        String url = WalmartConstant.BASE_URL + WalmartConstant.GET_PRODUCTS_US_URL;
        String request = WalmartRequest.request(url, Method.GET, accountId, paramMap);
        isPullCompleteProducts(request, productStrs, accountId, url, paramMap);
        return productStrs;
    }

    /**
     * 获取商品
     *
     * @param accountId 店铺ID
     * @author senghor
     * @date 2022/5/12 17:20
     */
    public static List<String> getProducts(Long accountId, List<String> skuList) {
        Map<String, Object> paramMap = new HashMap<>();
        List<String> productStrs = new ArrayList<>();
        DcWalmartSellers walmartSellers = WalmartRequest.getShopSettings(accountId);
        String urlUS = WalmartConstant.BASE_URL + WalmartConstant.GET_PRODUCTS_US_URL;
        String urlCA = WalmartConstant.BASE_URL + WalmartConstant.GET_PRODUCTS_CA_URL;
        for (String sku : skuList) {
            paramMap.put("sku", sku);
            if (walmartSellers.getSite().equals("US")) {
                paramMap.put("sku", sku);
                String json = null;
                try {
                    json = WalmartRequest.request(urlUS, Method.GET, accountId, paramMap);
                } catch (Exception e) {
                    log.error("Walmart 商品同步错误",e);
                }
                if (StringUtils.isNotEmpty(json)) {
                    productStrs.add(json);
                }
            } else if (walmartSellers.getSite().equals("CA")) {
                String json = WalmartRequest.request(urlCA, Method.GET, accountId, paramMap);
                if (StringUtils.isNotEmpty(json)) {
                    productStrs.add(json);
                }
            }
        }
        return productStrs;
    }


    /**
     * @param request     返回的单条数据
     * @param productStrs 返回数据集合
     */
    private static void isPullCompleteProducts(String request, List<String> productStrs, Long accountId, String url, Map<String, Object> paramMap) {
        productStrs.add(request);
        JSONObject json = JSONUtil.parseObj(request);
        String str = json.getStr("nextCursor");
        if (StringUtils.isNotEmpty(str)) {
            paramMap.put("nextCursor", str);
            String request1 = WalmartRequest.request(url, Method.GET, accountId, paramMap);
            if (StringUtils.isNotEmpty(request1)) {
                isPullCompleteProducts(request1, productStrs, accountId, url, paramMap);
            }
        }
        //return productStrs;
    }


    /**
     * 获取商品
     *
     * @param accountId 店铺ID
     * @author senghor
     * @date 2022/5/12 17:20
     */
    public static List<String> getProductsNum(Long accountId) {
        List<String> productStrs = new ArrayList<>();
//        for (String publishedStatus : PRODUCT_LIST) {
//            Map<String, Object> paramMap = new HashMap<>();
//            paramMap.put("status", publishedStatus);
//            String url = "https://marketplace.walmartapis.com/v3/items/count";
//            String request = WalmartRequest.request(url, Method.GET, accountId, paramMap);
//            productStrs.add(request);
//        }
//        paramMap.put("includeDetails", Boolean.TRUE);
        return productStrs;
        //orderStrs=isPullCompleteProducts(request,productStrs,accountId,url,paramMap);
    }


    /**
     * 获取商品库存测试
     *
     * @param accountId 店铺ID
     * @author senghor
     * @date 2022/5/12 17:20
     */
    public static List<String> getInventoryTest(Long accountId) {
        List<String> productStrs = new ArrayList<>();
        Map<String, Object> paramMap = new HashMap<>();
        //paramMap.put("sku", "(WMT-RP-BPM001A,WMT-RP-AP088W01)");
        String url = "https://marketplace.walmartapis.com/v3/inventories";
        String request = WalmartRequest.request(url, Method.GET, accountId, paramMap);
        productStrs.add(request);
        return productStrs;
        //orderStrs=isPullCompleteProducts(request,productStrs,accountId,url,paramMap);


    }


    /**
     * 获取商品库存测试
     *
     * @param accountId 店铺ID
     * @author senghor
     * @date 2022/5/12 17:20
     */
    public static List<String> getOrderTest(Long accountId) {
        String url = WalmartConstant.BASE_URL;
        List<String> list = new ArrayList<>();
        url = url + WalmartConstant.GET_ORDER_US_URL;
        for (Map<String, String> paramMap : PARAM_LIST) {
            paramMap.put("lastModifiedStartDate", "2022-11-04T14:00:00Z");
            paramMap.put("lastModifiedEndDate", "2022-11-05T14:00:00Z");
            // paramMap.put("purchaseOrderId", "3862024391012");  //采购订单号
            //paramMap.put("customerOrderId", "8472211820573");  //客户订单号

            paramMap.put("createdEndDate", "2022-11-09T00:00:00Z");  //这个创建之前的订单数据

            setCreatedStartDateByUS(paramMap, "2022-07-01T00:00:00Z");

            String json = WalmartRequest.request(url, Method.GET, accountId, paramMap);
            list.add(json);
        }
        return list;
        //orderStrs=isPullCompleteProducts(request,productStrs,accountId,url,paramMap);
    }


    /**
     * 获取商品库存测试
     *
     * @param accountId 店铺ID
     * @author senghor
     * @date 2022/5/12 17:20
     */
    public static List<String> getOrderTest1(Long accountId) {
        String url = WalmartConstant.BASE_URL;
        List<String> list = new ArrayList<>();
        url = url + WalmartConstant.GET_ORDER_US_URL;
        Map<String, String> paramMap = new HashMap<>();
        paramMap.put("shipNodeType", "WFSFulfilled");
        paramMap.put("limit", "200");
        paramMap.put("lastModifiedStartDate", "2022-11-04T00:00:00Z");
        paramMap.put("lastModifiedEndDate", "2022-11-05T00:00:00Z");
        // paramMap.put("purchaseOrderId", "3862024391012");  //采购订单号
        //paramMap.put("customerOrderId", "8472211820573");  //客户订单号

        paramMap.put("createdEndDate", "2022-11-04T00:00:00Z");  //这个创建之前的订单数据

        setCreatedStartDateByUS(paramMap, "2022-07-01T00:00:00Z");

        String json = WalmartRequest.request(url, Method.GET, accountId, paramMap);
        list.add(json);

        return list;
        //orderStrs=isPullCompleteProducts(request,productStrs,accountId,url,paramMap);
    }


    /**
     * 获取订单  测试
     *
     * @param accountId 店铺ID
     * @author senghor
     * @date 2022/5/12 17:20
     */
    public static String getOrderByPurchaseOrderId(Long accountId, DateTime dateTimeStart) {
        Map<String, String> paramMap = new HashMap<>();
        DcWalmartSellers walmartSellers = WalmartRequest.getShopSettings(accountId);
        String url = WalmartConstant.BASE_URL;
        paramMap.put("createdStartDate", "2022-04-30T16:00:00Z");
        paramMap.put("createdEndDate", "2022-07-30T16:00:00Z");

        DateTime dateTimeEnd = DateUtil.offsetDay(dateTimeStart, 49);

        paramMap.put("lastModifiedStartDate", dateTimeStart.toString(DatePattern.UTC_PATTERN));
        paramMap.put("lastModifiedEndDate", dateTimeEnd.toString(DatePattern.UTC_PATTERN));
        paramMap.put("shipNodeType", "WFSFulfilled");

        url = url + WalmartConstant.GET_ORDER_US_URL;
        String request = WalmartRequest.request(url, Method.GET, accountId, paramMap);

//        //ACKNOWLEDGED_MAP
//        Map<String, String> paramMap1 = ACKNOWLEDGED_MAP;
//
//        paramMap1.put("createdStartDate", "2022-04-30");
//        String request1 = WalmartRequest.request(url, Method.GET, accountId, paramMap1);

        return request;
    }

    public static String getOrderByPurchaseOrderId1(Long accountId, String purchaseOrderId, DateTime dateTimeStart) {
        Map<String, String> paramMap = new HashMap<>();
        DcWalmartSellers walmartSellers = WalmartRequest.getShopSettings(accountId);
        String url = WalmartConstant.BASE_URL;
        //paramMap.put("purchaseOrderId",purchaseOrderId);
        paramMap.put("lastModifiedStartDate", "2022-07-31T16:00:00Z");
        paramMap.put("lastModifiedEndDate", "2022-08-01T16:00:00Z");


        DateTime dateTimeEnd = DateUtil.offsetDay(dateTimeStart, 1);

        paramMap.put("createdStartDate", dateTimeStart.toString(DatePattern.UTC_PATTERN));
//        paramMap.put("lastModifiedEndDate", dateTimeEnd.toString(DatePattern.UTC_PATTERN));
        paramMap.put("shipNodeType", "WFSFulfilled");
        paramMap.put("status", "Delivered");

        url = url + WalmartConstant.GET_ORDER_US_URL;
        DateTime createdStartDate = new DateTime("2022-07-29T16:00:00Z");
        String request = "";
        request = WalmartRequest.request(url, Method.GET, accountId, paramMap);


//        do {
//            DateTime createdEndDate =DateUtil.offsetDay(createdStartDate,1);
//            paramMap.put("lastModifiedStartDate",createdStartDate.toString(DatePattern.UTC_PATTERN));
//            paramMap.put("lastModifiedEndDate", createdEndDate.toString(DatePattern.UTC_PATTERN));
//            createdStartDate =DateUtil.offsetDay(createdStartDate,1);
//
//            request = WalmartRequest.request(url, Method.GET, accountId, paramMap);
//        }while (StringUtils.isNotEmpty(request));
//        //ACKNOWLEDGED_MAP
//        Map<String, String> paramMap1 = ACKNOWLEDGED_MAP;
//
//        paramMap1.put("createdStartDate", "2022-04-30");
//        String request1 = WalmartRequest.request(url, Method.GET, accountId, paramMap1);

        return request;
    }


    public static String getOrderAll(Long accountId) {
//        Map<String, String> paramMap = SHIPPED_MAP;
//        DcWalmartSellers walmartSellers = WalmartRequest.getShopSettings(accountId);
//        String url = WalmartConstant.BASE_URL;
//        //paramMap.put("purchaseOrderId",purchaseOrderId);
//        paramMap.put("createdStartDate", "2022-04-30");
//        url = url + WalmartConstant.GET_ORDER_US_URL;
//        String request = WalmartRequest.request(url, Method.GET, accountId, paramMap);
//
//        //ACKNOWLEDGED_MAP
//        Map<String, String> paramMap1 = ACKNOWLEDGED_MAP;
//
//        paramMap1.put("createdStartDate", "2022-04-30");
//        String request1 = WalmartRequest.request(url, Method.GET, accountId, paramMap1);
        // https://marketplace.walmartapis.com/v3/orders?createdStartDate=2022-08-01T07:35:00Z&lastModifiedEndDate=2022-11-01T07:41:04Z&limit=200&
        // shipNodeType=WFSFulfilled&lastModifiedStartDate=2022-11-01T07:35:00Z
        Map<String, String> paramMap = new HashMap<>();
        paramMap.put("shipNodeType", "SellerFulfilled");
        paramMap.put("limit", LIMIT);
        String url = WalmartConstant.BASE_URL;
        //paramMap.put("purchaseOrderId",purchaseOrderId);

        paramMap.put("lastModifiedStartDate", "2022-10-31T08:53:00Z");
        paramMap.put("lastModifiedEndDate", "2022-11-01T08:53:00Z");
        paramMap.put("createdStartDate", "2022-08-01T07:35:00Z");

        url = url + WalmartConstant.GET_ORDER_US_URL;
        String request = WalmartRequest.request(url, Method.GET, accountId, paramMap);

        return request;
    }

    public static String getOrderByIdTest(String id) {
        List<String> list = new ArrayList<>();
        String url = "https://marketplace.walmartapis.com/v3/orders/" + id;
        return WalmartRequest.request(url, Method.GET, 60l, null);
    }
    public static String getOrderByPurchaseOrderId(String id) {
        String url = "https://marketplace.walmartapis.com/v3/orders/" + id;
        String json = WalmartRequest.request(url, Method.GET, 60l, null);
        return json;
    }


    public static List<String> getOrderByIdAndDateTest(String id) {
        List<String> list = new ArrayList<>();
        String url = "https://marketplace.walmartapis.com/v3/orders/" + id;

        Map<String, String> paramMap = new HashMap<>();
        paramMap.put("lastModifiedStartDate", "2022-11-02T07:35:00Z");
        paramMap.put("lastModifiedEndDate", "2022-11-03T07:50:00Z");

        String json = WalmartRequest.request(url, Method.GET, 60l, null);
        list.add(json);
        return list;
    }



    public static List<String> getOrderByIdAndDateTestCa(String id) {
        List<String> list = new ArrayList<>();
        String url = "https://marketplace.walmartapis.com/v3/ca/orders";

        Map<String, String> paramMap = new HashMap<>();
        //paramMap.put("createdEndDate", "2022-12-02T07:35:00Z");
        paramMap.put("createdStartDate", "2022-10-15T07:50:00Z");
        paramMap.put("limit", "200");

        String json = WalmartRequest.request(url, Method.GET, 59l, paramMap);
        list.add(json);
        return list;
    }



    public static void main(String[] args) {
        String lastModifiedEndDate = "2022-10-15T07:50:00Z";
        DateTime localTimeEnd = DateUtil.offsetMonth(new DateTime(lastModifiedEndDate), -1);
        System.out.println(localTimeEnd);
    }

    public static  JSONObject listItemsUs(DcWalmartItemParams itemParams, Long accountId) {
        String nextCursor = Optional.ofNullable(itemParams.getNextCursor()).orElse("*");
        String limit = Optional.ofNullable(itemParams.getLimit()).orElse("50");
        String urlUS = WalmartConstant.BASE_URL + WalmartConstant.GET_PRODUCTS_US_URL+"?nextCursor=" + nextCursor + "&limit=" + limit;

        String request = WalmartRequest.request(urlUS, Method.GET, accountId, null);
        if(StringUtils.isEmpty(request)){
            return null;
        }


        return  JSON.parseObject(request);
    }

    public static JSONObject listItemsCa(DcWalmartItemParams itemParams, Long accountId) {

        String nextCursor = Optional.ofNullable(itemParams.getNextCursor()).orElse("*");
        String limit = Optional.ofNullable(itemParams.getLimit()).orElse("50");
        String url = "https://marketplace.walmartapis.com/v3/ca/items?nextCursor=" + nextCursor + "&limit=" + limit;


        Map<String, Object> map = BeanUtil.beanToMap(itemParams);
        String request = WalmartRequest.request(url, Method.GET, accountId, null);
        if(StringUtils.isEmpty(request)){
            return null;
        }


        return  JSON.parseObject(request);
    }
}
