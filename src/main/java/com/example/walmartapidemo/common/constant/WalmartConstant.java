package com.example.walmartapidemo.common.constant;

/**
 * 沃尔玛常量
 *
 * @author senghor
 * @date 2022/5/12 16:42
 */
public class WalmartConstant {
    // redis key
    public static final String SHOP_SETTINGS = "shop:walmart:";
    // 基础url
    public static final String BASE_URL = "https://marketplace.walmartapis.com";
    // 沙盒url
    public static final String BASE_SANDBOX_URL = "https://sandbox.walmartapis.com";
    // 请求token的url
    public static final String USA_TOKEN_URL = "/v3/token";
    // 获取可用的报告日期列表url
    public static final String AVAILABLE_RECON_REPORT_DATES_URL = "/v3/report/reconreport/availableReconFiles?reportVersion=v1";
    // 生成报告url
    public static final String RECON_REPORT_URL = "/v3/report/reconreport/reconFile";


    // 获取报告url
    public static final String GET_REPORT_URL = "/v3/getReport";
//    // 获取订单详情url-US用
//    public static final String GET_ORDER_US_URL = "/v3/orders/{purchaseOrderId}";
//    // 获取订单详情url-CA用
//    public static final String GET_ORDER_CA_URL = "/v3/ca/orders/{purchaseOrderId}";
    // 获取订单详情url-US用
    public static final String GET_ORDER_US_URL = "/v3/orders";
    // 获取订单详情url-CA用
    public static final String GET_ORDER_CA_URL = "/v3/ca/orders";
    //产品
    public static final String GET_PRODUCTS_US_URL = "/v3/items";
    //产品-CA用
    public static final String GET_PRODUCTS_CA_URL = "/v3/ca/items";


    //US在售库存  https://marketplace.walmartapis.com/v3/inventory?sku= + sku
    public static final String GET_INVENTORY_US_URL = "/v3/inventory?sku=";
    //CA在售库存  https://marketplace.walmartapis.com/v3/ca/inventory?sku= + sku;
    public static final String GET_INVENTORY_CA_URL = "/v3/ca/inventory?sku=";
    //US 可售数量
    public static final String GET_INVENTORY_FULFILLMENT_URL = "/v3/fulfillment/inventory";


}
