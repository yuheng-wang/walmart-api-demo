package com.example.walmartapidemo.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * walmart店铺授权配置表
 * </p>
 *
 * @author morris
 * @since 2023-07-06
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("dc_walmart_sellers")
public class DcWalmartSellers implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 店铺账号id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 店铺账号
     */
    @TableField("account_name")
    private String accountName;

    /**
     * 货币code
     */
    @TableField("currency_code")
    private String currencyCode;

    /**
     * 地区
     */
    @TableField("site")
    private String site;

    /**
     * 时区
     */
    @TableField("time_zone")
    private String timeZone;

    /**
     * key
     */
    @TableField("app_key")
    private String appKey;

    /**
     * 密钥
     */
    @TableField("app_secret")
    private String appSecret;

    /**
     * 沃尔玛加拿大站点渠道类型
     */
    @TableField("channel_type")
    private String channelType;

    /**
     * 沃尔玛加拿大站点CONSUMER_ID
     */
    @TableField("consumer_id")
    private String consumerId;

    /**
     * 沃尔玛加拿大站点
     */
    @TableField("private_key")
    private String privateKey;

    /**
     * 店铺状态  0-禁用 1-启用  
     */
    @TableField("status")
    private Boolean status;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 更新时间
     */
    @TableField(value = "last_update_time", fill = FieldFill.INSERT_UPDATE)
    private Date lastUpdateTime;

    /**
     * 店铺顺序
     */
    @TableField("sort")
    private Integer sort;

    /**
     * 备注
     */
    @TableField("remark")
    private String remark;


}
