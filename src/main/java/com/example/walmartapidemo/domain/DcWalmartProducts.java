package com.example.walmartapidemo.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * walmart商品源数据表
 * </p>
 *
 * @author morris
 * @since 2023-07-06
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("dc_walmart_products")
public class DcWalmartProducts implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 平台+站点
     */
    @TableField("mart")
    private String mart;

    /**
     * 销售sku
     */
    @TableField("sku")
    private String sku;

    /**
     * wp id
     */
    @TableField("wpid")
    private String wpid;

    /**
     * 全球贸易项目代码	
     */
    @TableField("gtin")
    private String gtin;

    /**
     * 商品名称
     */
    @TableField("product_name")
    private String productName;

    /**
     * 货架
     */
    @TableField("shelf")
    private String shelf;

    /**
     * 商品类型
     */
    @TableField("product_type")
    private String productType;

    /**
     * 价格货币
     */
    @TableField("price_currency")
    private String priceCurrency;

    /**
     * 价格
     */
    @TableField("price_amount")
    private BigDecimal priceAmount;

    /**
     * 商品状态
     */
    @TableField("published_status")
    private String publishedStatus;

    /**
     * 生命周期状态
     */
    @TableField("lifecycle_status")
    private String lifecycleStatus;

    /**
     * 变体分组ID
     */
    @TableField("variant_group_id")
    private String variantGroupId;

    /**
     * 是不是主要变体 0 不是 1是
     */
    @TableField("variant_is_primary")
    private Boolean variantIsPrimary;

    /**
     * 分组属性
     */
    @TableField("variant_grouping_attributes")
    private String variantGroupingAttributes;

    /**
     * 分组数量
     */
    @TableField("variant_group_num")
    private Integer variantGroupNum;

    /**
     * sellers ID
     */
    @TableField("sellers_id")
    private Long sellersId;

    /**
     * 站点
     */
    @TableField("site")
    private String site;

    @TableField("available_quantity")
    private Integer availableQuantity;

    @TableField("quantity_on_sale")
    private Integer quantityOnSale;
}
