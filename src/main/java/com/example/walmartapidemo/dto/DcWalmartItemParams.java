package com.example.walmartapidemo.dto;

import lombok.Data;

/**
 * @author Charles
 * @date 2021/10/18
 */
@Data
public class DcWalmartItemParams {
    /**
     * 默认值： “*”
     * 用于检索超过 200 个项目时的分页。响应中收到的 nextCursor 值对于所有后续页面请求将相同
     */
    private String nextCursor = "*";
    private String sku;
    private String offset;
    private String limit;
    /**
     * 生命周期状态:允许的值有 ACTIVE 、 ARCHIVED 、 RETIRED
     * 加拿大站点无此属性
     */
    private String lifecycleStatus;
    /**
     * 发布状态：允许的值有 PUBLISHED、UNPUBLISHED
     * 加拿大站点无此属性
     */
    private String publishedStatus;

    private Long accountId;
}
