package com.example.walmartapidemo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.walmartapidemo.domain.DcWalmartProducts;

/**
 * @author Charles
 * @date 2024/2/2
 */
public interface WalmartProductsService extends IService<DcWalmartProducts> {

    void walmartSaveProducts();
}
