package com.example.walmartapidemo.service.impl;

import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.walmartapidemo.common.utils.WalmartUtils;
import com.example.walmartapidemo.domain.DcWalmartProducts;
import com.example.walmartapidemo.domain.DcWalmartSellers;
import com.example.walmartapidemo.dto.DcWalmartItemParams;
import com.example.walmartapidemo.mapper.WalmartProductsMapper;
import com.example.walmartapidemo.mapper.WalmartSellersMapper;
import com.example.walmartapidemo.service.WalmartProductsService;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Charles
 * @date 2024/2/2
 */
public class WalmartProductsServiceImpl extends ServiceImpl<WalmartProductsMapper, DcWalmartProducts> implements WalmartProductsService {
    private static final Integer BATCH_SIZE = 500;
    private static final String US = "US";
    private static final String CA = "CA";

    @Resource
    private WalmartSellersMapper walmartSellersMapper;

    @Override
    public void walmartSaveProducts() {
        List<DcWalmartProducts> products = this.list(new QueryWrapper<DcWalmartProducts>().lambda().select(DcWalmartProducts::getId, DcWalmartProducts::getSku));
        Map<String, Long> keyMap = products.stream().collect(Collectors.toMap(DcWalmartProducts::getSku, DcWalmartProducts::getId, (k1, k2) -> k1));

        //美国站点
        List<DcWalmartSellers> sellersList = new LambdaQueryChainWrapper<>(walmartSellersMapper).list();
        List<DcWalmartProducts> productList = new ArrayList<>();
        for (DcWalmartSellers shopSiteVo : sellersList) {
            //1.商品表数据同步
            Long accountId = shopSiteVo.getId();
            String siteName = shopSiteVo.getSite();
            DcWalmartItemParams itemParams = new DcWalmartItemParams();
            itemParams.setLimit("50");
            String nextCursor = "*";
            if ("US".equals(siteName)) {
                //美国站点
                List<DcWalmartProducts> productListUS = new ArrayList<>();
                //同步商品信息
                while (nextCursor != null) {
                    itemParams.setNextCursor(nextCursor);
                    JSONObject jsonObject = WalmartUtils.listItemsUs(itemParams, accountId);
                    if (jsonObject == null) {
                        break;
                    }
                    if (jsonObject.getInt("totalItems") != 0) {
                        JSONArray array = jsonObject.getJSONArray("ItemResponse");
                        for (Object o : array) {
                            JSONObject orderLines = (JSONObject) o;
                            String sku = orderLines.getStr("sku");
                            orderLines.put("id",keyMap.get(sku));
//                            DcWalmartProductTransform.productsToOrder(orderLines, productListUS, shopSiteVo);
                        }
                    }
                    productList.addAll(productListUS);
                    nextCursor = jsonObject.getStr("nextCursor");
                }

            } else if ("CA".equals(siteName)) {
                List<DcWalmartProducts> productListAC = new ArrayList<>();
                //加拿大站点
                while (nextCursor != null) {
                    itemParams.setNextCursor(nextCursor);
                    JSONObject jsonObject = WalmartUtils.listItemsCa(itemParams, accountId);
                    if (jsonObject == null) {
                        break;
                    }
                    if (jsonObject.getInt("totalItems") != 0) {
                        JSONArray array = jsonObject.getJSONArray("ItemResponse");
                        for (Object o : array) {
                            JSONObject orderLines = (JSONObject) o;
                            String sku = orderLines.getStr("sku");
                            orderLines.put("id",keyMap.get(sku));
//                            DcWalmartProductTransform.productsToOrder(orderLines, productListAC, shopSiteVo);
                        }
                    }
                    productList.addAll(productListAC);
                    nextCursor = jsonObject.getStr("nextCursor");
                }
            }
        }

        if (CollectionUtils.isNotEmpty(productList)) {
            this.saveOrUpdateBatch(productList, BATCH_SIZE);
        }
    }
}
